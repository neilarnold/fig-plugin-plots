﻿
$j = jQuery.noConflict();

$j(document).ready(
  function () {
  	console.log("FIG Plot JS Ready.");

  	$j(".detail-picture-post img").hide();
  	$j(".table-picture-post img").mouseover(function (e) {
  		$j(".table-picture-post img").removeClass("border");
  		$j(this).addClass("border");
  		src = $j(this).data("large");
  		showPicturePost(src);
  	});
  	$j(".table-picture-post img").click(function (e) {
  		$j(".table-picture-post img").removeClass("border");
  		$j(this).addClass("border");
  		src = $j(this).data("large");
  		showPicturePost(src);
  	});

  }
);

function showPicturePost(src) {
	//console.log("Showing: " + src);
	$j(".detail-picture-post img").fadeIn();
	$j(".detail-picture-post img").attr("src", src);
}