﻿
$j = jQuery.noConflict();

$j(window).load(
	function () {
  		console.log("FIG Front-End Form JS Ready.");

		$j("form").submit(function() { window.onbeforeunload = null; });

  		$j("._post_meta_fig_tree_fig_tree_dbh").live('keyup', function () {

  			var species_type = $j(this).parent().closest(":has(._post_meta_fig_tree_fig_tree_type)").find("._post_meta_fig_tree_fig_tree_type").val();
  			var species_id = $j(this).parent().closest(":has(._post_meta_fig_tree_fig_tree_" + species_type + ")").find("._post_meta_fig_tree_fig_tree_" + species_type + "").val();
  			//var species_calc = $j(this).parent().closest(":has(._post_meta_fig_tree_fig_tree_" + species_type + "_calcs)").find("._post_meta_fig_tree_fig_tree_" + species_type + "_calcs option[value='" + species_id + "']").text();
			var species_calc = $j("._post_meta_fig_tree_" + species_type + "_calcs option[value='" + species_id + "']").text();
  			var dbh = $j(this).parent().closest(":has(._post_meta_fig_tree_fig_tree_dbh)").find("._post_meta_fig_tree_fig_tree_dbh").val();

  			var predicted = getCalc(dbh, species_calc);
  			$j(this).parent().closest(":has(._post_meta_fig_tree_fig_tree_height_predicted)").find("._post_meta_fig_tree_fig_tree_height_predicted").val(predicted);

  			//console.log("species_type: " + species_type);
  			//console.log("species_id: " + species_id);
  			//console.log("species_calc: " + species_calc);
  			//console.log("dbh: " + dbh);
  			//console.log("Predicted: " + predicted);
  		});

  		$j(".piklist-addmore-remove").live('click', function () {
  			if (confirm('Do you wish to remove this tree?'))
  				return true;
  			else
  				return false;
  		});
        $j(".piklist-addmore-add").live('click', function () {
            $j("._post_meta_fig_tree_fig_tree_type").change(); //refresh on load.
        });

		$j("._post_meta_fig_tree_fig_tree_type").live('change', function() {
            var species_type = $j(this).val();
            $j(this).parent().closest(":has(._post_meta_fig_tree_fig_tree_softwood)").find("._post_meta_fig_tree_fig_tree_softwood").hide().parent().addClass("container-hide");
            $j(this).parent().closest(":has(._post_meta_fig_tree_fig_tree_hardwood)").find("._post_meta_fig_tree_fig_tree_hardwood").hide().parent().addClass("container-hide");
            $j(this).parent().closest(":has(._post_meta_fig_tree_fig_tree_" + species_type + ")").find("._post_meta_fig_tree_fig_tree_" + species_type + "").show().parent().removeClass("container-hide");
			//console.log("val: " + $j(this).val());
		});
        $j("._post_meta_fig_tree_fig_tree_type").change(); //refresh on load.


	}

);

window.onbeforeunload = function() {
	return "Are you sure you want to leave the form? You will loose all the data that you have not submitted yet.";
}


function getCalc(dbh, calc) {
	//console.log(calc);
	var converted_calc = calc.toLowerCase().replace("dbh", dbh);

	//console.log(converted_calc);
	return math.format(math.eval(converted_calc), { notation: 'fixed', precision: 2 } );
}
