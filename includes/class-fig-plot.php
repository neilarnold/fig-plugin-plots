<?php
/*
 * Access Database Utils
 * Version: 0.1
 *
 * Various tools for working with Access databases
 *
 */

 if (!defined('ABSPATH')) exit; // Exit if accessed directly


if (!class_exists('FigPlot')) {
	class FigPlot
	{
		// Static Functions --- --- ---
		public static function GetPlotPost($fig_slug) {
			$args = array(
				'name'        => $fig_slug,
				'post_type'   => 'fig-plot',
	            'post_status' => 'publish',
				'numberposts' => 1
			);
			$my_posts = get_posts($args);
			if ( $my_posts )
				return $my_posts[0];
		}

		public function GetAllPlots() {
			$args = array(
				'post_type' => 'fig-plot',
				'orderby' => array( 'post_title' => 'ASC' ),
				'nopaging' => true
			);
			$query = new WP_Query($args);

			return $query;
		} // GetAllPlots

		public function GetDataByPlot($plotid) {
			$args = array(
				'post_type' => 'fig-survey',
				'meta_query' => array(
				   array(
					   'key' => 'fig_survey_of_plot',
					   'value' => $plotid,
					   'compare' => 'EXISTS',
				   )
			   )
			);
			$query = new WP_Query($args);

			//echo "Found for Plot " . $plotid . ": " . $query->found_posts;
			return $query;
		} // GetDataYears --- --- ---

		public function GetDataByYear($plotid, $year) {
			$args = array(
				'post_type' => 'fig-survey',
				'nopaging' => true,
				'meta_query' => array(
					'relation' => 'AND',
					array(
					   'key' => 'fig_survey_of_plot',
					   'value' => $plotid,
					   'compare' => 'EXISTS',
					),
					array(
						'key' => 'fig_survey_date',
						'value' => ($year.'-01-01'),
						'compare' => '>=',
						'type' => 'DATE',
					),
					array(
					    'key' => 'fig_survey_date',
						'value' => ($year.'-12-31'),
					    'compare' => '<=',
					    'type' => 'DATE',
					),


			   )
			);
			$query = new WP_Query($args);

			//echo "Found: " . $query->found_posts;
			return $query;
		} // GetDataByYear --- --- ---

		public function GetYearsOfData($plotid) {
			$args = array(
				'post_type' => 'fig-survey',
	            'post_status' => 'publish',
				'nopaging' => true,
				'meta_query' => array(
					'relation' => 'AND',
					array(
					   'key' => 'fig_survey_of_plot',
					   'value' => $plotid,
					   'compare' => 'EXISTS',
					)
					,'date_clause' => array(
					   'key' => 'fig_survey_date',
					   'compare' => 'EXISTS',
				   ),

			   ),
			   'orderby' => 'city_clause'
			);
			$query = new WP_Query($args);

			$years = array();
			$current_year = '';
			while ( $query->have_posts() ) {
				$query->the_post();

				$survey_year = date('Y', strtotime(get_post_meta($query->post->ID, 'fig_survey_date', true)));
				if ($current_year != $survey_year) {
					$current_year = $survey_year;
					$years[] = $current_year;
					//echo $current_year . '-<br />';
				}

			}

			return $years;
		} // GetDataYears --- --- ---



	} // class FigPlot --- --- ---

} // if (!class_exists('FigPlot')) --- --- ---
