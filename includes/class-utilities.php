<?php
/*
 * FIG Utils
 * Version: 0.1
 *
 * Various tools for working with FIG
 *
 */
 
 if (!defined('ABSPATH')) exit; // Exit if accessed directly
 
 
if (!class_exists('FigUtils')) {
	class FigUtils
	{
		public static function ShowWithLabel($label, $value) {
			
			if ($value != '')
				return '<p>' . $label . ': <b>' . $value . '</b></p>';
			return '';
		}

		public static function ShowLongLat($label, $array) {
			if (!empty($array))
				return '<p>' . $label . ': <b>' . $array['fig_plot_latitude'] . ', ' . $array['fig_plot_longitude'] . '</b></p>';
			return '';
		}

		public static function ShowImage($attachment_id) {
			$img = '';
			
			if (isset($attachment_id) || !empty($attachment_id)) {
				
				$thumb = wp_get_attachment_thumb_url($attachment_id);
				$main = wp_get_attachment_image_src($attachment_id, 'large')[0];

				if ($thumb)
					$img = '<img src="' . $thumb . '" data-large="' . $main . '" />';
			}

			return $img;

		}
	}
}