<?php
/*
Title: Plot Information
Method: post
Message: Your plot has been updated.
Logged in: false
NOT CURRENTLY USED...
*/

piklist('field', array(
	'type' => 'hidden'
	,'scope' => 'post'
	,'field' => 'post_type'
	,'value' => 'fig'
));

piklist('field', array(
	'type' => 'text'
	,'scope' => 'post' // post_title is in the wp_posts table, so scope is: post
	,'field' => 'post_title'
	,'label' => __('Title', 'fig')
	,'attributes' => array(
		'wrapper_class' => 'post_title'
		,'style' => 'width: 100%'
	)
));

piklist('field', array(
	'type' => 'hidden'
	,'scope' => 'post'
	,'field' => 'post_status'
	,'value' => 'pending'
));



piklist('field', array(
	'type' => 'select'
	,'scope' => 'post_meta' // Not used for settings sections
	,'field' => 'fig_post_bioregion'
	,'label' => __('Bioregion')
	,'attributes' => array(
		'class' => 'regular-text'
	)
	,'choices' => array(
		'Aroostook Hills and Lowlands' => 'Aroostook Hills and Lowlands'
		,'Central Interior' => 'Central Interior'
		,'Central and Western Mountains' => 'Central and Western Mountains'
		,'Downeast Maine' => 'Downeast Maine'
		,'Eastern Lowlands' => 'Eastern Lowlands'
		,'Midcoast - Penobscot Bay' => 'Midcoast - Penobscot Bay'
		,'Northwest Maine' => 'Northwest Maine'
		,'South Maine' => 'South Maine'
	)
));

piklist('field', array(
	'type' => 'group'
	,'scope' => 'post_meta' // Not used for settings sections
	,'field' => 'fig_tree'
	,'label' => __('Tree')
	,'columns' => 12
	,'add_more' => true
	,'fields' => array(
		array(
			'type' => 'text'
			,'scope' => 'post_meta' // Not used for settings section
			,'field' => 'fig_tree_marker'
			,'columns' => 4
            ,'attributes' => array(
				'class' => 'regular-text',
				'placeholder' => 'Marker #'
            )
		)
		,array(
			'type' => 'select'
			,'scope' => 'post_meta' // Not used for settings section
			,'field' => 'fig_tree_species'
			,'columns' => 8
			,'choices' => array(
				'' => '[Species]'
				,'cedar' => 'Cedar'
				,'spruce' => 'Spruce'
				,'white-pine' => 'White Pine'
				,'yellow-birch' => 'Yellow Birch'
			)
            ,'attributes' => array(
				'class' => 'regular-text',
            )
		)

		,array(
			'type' => 'group'
			,'scope' => 'post_meta' // Not used for settings section
			,'field' => 'fig_tree_details'
			,'add_more' => true
			,'fields' => array(
				array(
					'type' => 'select'
					,'scope' => 'post_meta' // Not used for settings section
					,'field' => 'fig_tree_status'
					,'columns' => 4
					,'choices' => array(
						'' => '[Status]'
						,'1' => 'Live'
						,'2' => 'Dead Standing'
						,'3' => 'Dead Down'
						,'4' => 'Dead Harvested'
					)
					,'attributes' => array(
						'class' => 'regular-text',
					)
				)
				,array(
					'type' => 'text'
					,'scope' => 'post_meta' // Not used for settings section
					,'field' => 'fig_tree_height'
					,'columns' => 4
					,'attributes' => array(
						'placeholder' => 'Height, in feet'
					)
					,'validate' => array(
						array(
							'type' => 'range'
							,'options' => array(
								'min' => 1
								,'max' => 400
							)
						)
					)
				)
				,array(
					'type' => 'text'
					,'scope' => 'post_meta' // Not used for settings section
					,'field' => 'fig_tree_dbh'
					,'columns' => 4
					,'attributes' => array(
						'placeholder' => 'DBH, in inches'
					)
					,'validate' => array(
						array(
							'type' => 'range'
							,'options' => array(
								'min' => 1
								,'max' => 500
							)
						)
					)
				)

				,array(
					'type' => 'text'
					,'scope' => 'post_meta' // Not used for settings section
					,'field' => 'fig_tree_dmg_type_crown'
					,'columns' => 3
					,'attributes' => array(
						'placeholder' => 'Dmg Type Crown'
					)
				)
				,array(
					'type' => 'text'
					,'scope' => 'post_meta' // Not used for settings section
					,'field' => 'fig_tree_dmg_pct_crown'
					,'columns' => 3
					,'attributes' => array(
						'placeholder' => 'Dmg % Crown'
					)
				)
				,array(
					'type' => 'text'
					,'scope' => 'post_meta' // Not used for settings section
					,'field' => 'fig_tree_dmg_type_bole'
					,'columns' => 3
					,'attributes' => array(
						'placeholder' => 'Dmg Type Bole'
					)
				)
				,array(
					'type' => 'text'
					,'scope' => 'post_meta' // Not used for settings section
					,'field' => 'fig_tree_dmg_pct_bole'
					,'columns' => 3
					,'attributes' => array(
						'placeholder' => 'Dmg % Bole'
					)
				)
				,array(
					'type' => 'datepicker'
					,'scope' => 'post_meta' // Not used for settings section
					,'field' => 'fig_tree_date'
					,'columns' => 6
					,'attributes' => array(
						'class' => 'text'
						,'placeholder' => 'Date of Measurement'
					)
					,'options' => array(
						'dateFormat' => 'M d, yy'
						,'firstDay' => '0'
					)
				)

			)
		)
	)
));

piklist('field', array(
    'type' => 'submit'
    ,'field' => 'submit'
    ,'value' => 'Submit'
));
