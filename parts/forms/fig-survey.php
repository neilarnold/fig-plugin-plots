<?php
/*
Title: Plot Information
Method: post
Message: Your plot has been updated.
Logged in: false
*/

wp_enqueue_script( 'math-js', plugin_dir_url( __FILE__ ) . '../../includes/js/math.min.js', array(), '2.5.0', true );
wp_enqueue_script( 'fig-form-js', plugin_dir_url( __FILE__ ) . '../../includes/js/front-end-form.js', array(), '2.5.0', true );
wp_enqueue_style( 'fig-files', plugin_dir_url( __FILE__ ) . '../../includes/css/front-end.css' );

$softwood = get_terms('fig-survey-softwood', array( 'hide_empty' => false));
$hardwood = get_terms('fig-survey-hardwood', array( 'hide_empty' => false));
$fig_suvey_status = get_terms('fig-survey-status', array( 'hide_empty' => false));

$my_plots = get_posts(array(
			  'post_type'		=> 'fig-plot'
			  ,'author'       =>  $current_user->ID
			  ,'numberposts'	=> -1
			  ,'orderby'		=> 'title'
			  ,'order'		=> 'ASC'
			));

// if (count($my_plots); < 1) {
// 	wp_redirect( get_permalink( 10 ));
// 	exit();
// }
//echo "Plot Count: " . count($my_plots) . '<br />';


if (!is_user_logged_in ()) {
    $redirect = '<script type="text/javascript">';
    $redirect .= 'window.location = "' . get_permalink( 10 ) . '"';
    $redirect .= '</script>';
	echo $redirect;
}

if (count($my_plots) < 1) {
    $redirect = '<script type="text/javascript">';
    $redirect .= 'window.location = "' . get_permalink( 10 ) . '"';
    $redirect .= '</script>';
	echo $redirect;
}

// Standard Post
piklist('field', array(
	'type' => 'hidden'
	,'scope' => 'post'
	,'field' => 'post_type'
	,'value' => 'fig-survey'
));

piklist('field', array(
	'type' => 'hidden'
	,'scope' => 'post'
	,'field' => 'post_status'
	,'value' => 'publish'
));

piklist('field', array(
    'type' => 'select'
	,'scope' => 'post_meta' // Not used for settings sections
    ,'field' => 'fig_survey_of_plot'
    ,'label' => 'Choose Your Plot'
    ,'choices' => piklist(
		  $my_plots
		  ,array('ID', 'post_title')
    )
    ,'relate' => array(
      'scope' => 'fig-plot'
    )
));

piklist('field', array(
	'type' => 'text'
	,'scope' => 'post' // post_title is in the wp_posts table, so scope is: post
	,'field' => 'post_title'
	,'label' => __('Survey Name', 'fig-survey')
	,'description' => __('Use a descriptive name like your school name and the year (i.e. Washington Academy 2016)')
	,'required' => true
	,'attributes' => array(
		'wrapper_class' => 'post_title'
		,'style' => 'width: 100%'
	)
));

piklist('field', array(
	'type' => 'editor'
	,'scope' => 'post' // post_title is in the wp_posts table, so scope is: post
	,'field' => 'post_content'
	,'label' => __('Field Notes', 'fig-survey')
	,'attributes' => array(
		'wrapper_class' => 'post_content'
		,'style' => 'width: 100%'
	)
));


// FIG Suvey Information
piklist('field', array(
	'type' => 'datepicker'
	,'scope' => 'post_meta' // Not used for settings sections
	,'field' => 'fig_survey_date'
	,'label' => 'Date'
	,'required' => true
	,'description' => '(YYYY-MM-DD) Date of the first date of the data collection event'
	,'attributes' => array(
		'class' => 'regular-text'
	)
	,'options' => array(
		'dateFormat' => 'yy-mm-dd'
		,'firstDay' => '0'
	)
	,'validate' => array(
		array(
			'type' => 'date'
		)
	)
));

piklist('field', array(
	'type' => 'select'
	,'scope' => 'post_meta'
	,'field' => 'fig-survey-weather'
	,'label' => 'Weather'
	,'choices' => array( '' => 'Choose Weather'  )
	    + piklist(get_terms('fig-survey-weather', array( 'hide_empty' => false))
	    ,array( 'term_id','name' )
	)
	,'attributes' => array(
		'class' => 'regular-text'
	)
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'fig_suvery_photo_north'
	,'scope' => 'post_meta'
	,'label' => __('Photo Journal - North','piklist')
	,'attributes' => array(
		'wrapper_class' => 'photo-upload x-column x-sm x-1-2'
	)
	,'options' => array(
		'basic' => true
		,'button' => 'Add Photo'
	)
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'fig_suvery_photo_east'
	,'scope' => 'post_meta'
	,'label' => __('Photo Journal - East','piklist')
	,'attributes' => array(
		'wrapper_class' => 'photo-upload x-column x-sm x-1-2 last'
	)
	,'options' => array(
		'basic' => true
		,'button' => 'Add Photo'
	)
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'fig_suvery_photo_south'
	,'scope' => 'post_meta'
	,'label' => __('Photo Journal - South','piklist')
	,'attributes' => array(
		'wrapper_class' => 'photo-upload x-column x-sm x-1-2'
	)
	,'options' => array(
		'basic' => true
		,'button' => 'Add Photo'
	)
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'fig_suvery_photo_west'
	,'scope' => 'post_meta'
	,'label' => __('Photo Journal - West','piklist')
	,'attributes' => array(
		'wrapper_class' => 'photo-upload x-column x-sm x-1-2 last'
	)
	,'options' => array(
		'basic' => true
		,'button' => 'Add Photo'
	)
));


// FIG Tree Information

piklist('field', array(
	'type' => 'select'
	,'scope' => 'post_meta'
	,'field' => 'fig_tree_softwood_calcs'
	,'choices' => piklist($softwood
		,array( 'term_id','description' )

	)
	,'attributes' => array(
		'wrapper_class' => 'container-hide',
		'class' => 'regular-text',
		'style' => 'display:none;'
	)
));

piklist('field', array(
	'type' => 'select'
	,'scope' => 'post_meta'
	,'field' => 'fig_tree_hardwood_calcs'
	,'choices' => piklist($hardwood
		,array( 'term_id','description' )

	)
	,'attributes' => array(
		'wrapper_class' => 'container-hide',
		'class' => 'regular-text',
		'style' => 'display:none;'
	)
));


piklist('field', array(
	'type' => 'group'
	,'scope' => 'post_meta'
	,'field' => 'fig_tree'
	,'label' => __('Tree')
	,'columns' => 12
	,'add_more' => true
	,'fields' => array(

		array(
			'type' => 'text'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_marker'
			,'columns' => 4
            ,'attributes' => array(
				'class' => 'regular-text',
				'placeholder' => 'Marker #'
            )
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta' // Not used for settings sections
			,'field' => 'fig_tree_type'
			,'columns' => 8
			//,'label' => 'Tree Type'
			//,'list' => false
			,'attributes' => array(
				'class' => 'text'
			)
			,'choices' => array(
				'' => 'Choose Tree Type'
				,'softwood' => 'Softwood'
				,'hardwood' => 'Hardwood'
			)
			//,'value' => 'softwood' // Sets default

		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_softwood'
			,'columns' => 12
			,'choices' => array( '' => 'Choose Softwood'  )
				+ piklist($softwood ,array( 'term_id','name' ))
			,'attributes' => array(
				'class' => 'regular-text'
			)
			// ,'conditions' => array(
			// 	array(
			// 		'field' => 'fig_tree:fig_tree_type'
			// 		,'value' => 'softwood'
			// 	)
			// )
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_hardwood'
			,'columns' => 12
			,'choices' => array( '' => 'Choose Hardwood'  )
				+ piklist($hardwood,array( 'term_id','name' ))
			,'attributes' => array(
				'class' => 'regular-text'
			)
			// ,'conditions' => array(
			// 	array(
			// 		'field' => 'fig_tree:fig_tree_type'
			// 		,'value' => 'hardwood'
			// 	)
			// )
		)

		,array(
			'type' => 'select'
			,'scope' => 'taxonomy'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_status'
			,'columns' => 12
			,'label' => 'Tree Status'
			,'choices' => array( '' => 'Choose Status'  )
				+ piklist($fig_suvey_status,array( 'term_id','name' ))
		)

		,array(
			'type' => 'text'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dbh'
			,'label' => 'DBH'
			,'Description' => 'in inches'
			,'columns' => 4
			,'attributes' => array(
				'placeholder' => 'DBH, in inches'
			)
			,'validate' => array(
				array(
					'type' => 'integer'
					,'options' => array( 'decimals' => 1 )
                )
				,array(
					'type' => 'range'
					,'options' => array(
						'min' => 1
						,'max' => 500
					)
				)
			)
		)

		,array(
			'type' => 'text'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_height_predicted'
			,'label' => 'Predicted Tree Height'
			,'Description' => 'in feet'
			,'columns' => 4
			,'attributes' => array(
				'placeholder' => 'Height, in feet'
				,'readonly' => 'readonly'
			)
		)

		,array(
			'type' => 'text'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_height'
			,'columns' => 4
			,'label' => 'Actual Tree Height'
			,'Description' => 'in feet'
			,'attributes' => array(
				'placeholder' => 'Height, in feet'
			)
			,'validate' => array(
				array(
					'type' => 'integer'
					,'options' => array( 'decimals' => 0 )
                )
				,array(
					'type' => 'range'
					,'options' => array(
						'min' => 1
						,'max' => 400
					)
				)
			)
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dmg_crown_type'
			,'label' => __('Damage Type Crown')
			,'columns' => 3
			,'attributes' => array( 'class' => 'regular-text' )
			,'choices' => array(
				'' => ''
				,'none' => 'None'
				,'branches' => 'Branches'
				,'foliage' => 'Foliage'
				,'both' => 'Both'
			)
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dmg_crown_pct'
			,'label' => __('Damage % Crown')
			,'columns' => 3
			,'attributes' => array( 'class' => 'regular-text' )
			,'choices' => array(
				'' => ''
				,'0-25' => '0-25%'
				,'25-50' => '25-50%'
				,'51-75' => '51-75%'
				,'76-100' => '76-100%'
			)
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dmg_bole_type'
			,'label' => __('Damage Type Bole')
			,'columns' => 3
			,'attributes' => array( 'class' => 'regular-text' )
			,'choices' => array(
				'' => ''
				,'none' => 'None'
				,'insects' => 'Insects'
				,'disease' => 'Disease'
				,'mechanical' => 'Mechanical'
				,'weather' => 'Weather'
				,'all' => 'All'
				,'other' => 'Other'
			)
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dmg_bole_pct'
			,'label' => __('Damage % Bole')
			,'columns' => 3
			,'attributes' => array( 'class' => 'regular-text' )
			,'choices' => array(
				'' => ''
				,'0-25' => '0-25%'
				,'25-50' => '25-50%'
				,'51-75' => '51-75%'
				,'76-100' => '76-100%'
			)
		)


	)
));


// Submit Button
piklist('field', array(
    'type' => 'submit'
    ,'field' => 'submit'
    ,'value' => 'Submit'
));
