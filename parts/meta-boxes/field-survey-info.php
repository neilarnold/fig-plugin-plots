<?php
/*
Title: Survey Information
Post Type: fig-survey
Context: normal
Priority: high
Order: 1
*/

piklist('field', array(
    'type' => 'select'
    ,'field' => 'fig_survey_of_plot'
    ,'label' => 'Survey of Plot'
    ,'choices' => piklist(
		  get_posts(array(
			'post_type'		=> 'fig-plot'
			,'numberposts'	=> -1
			,'orderby'		=> 'title'
			,'order'		=> 'ASC'
		  ))
		  ,array('ID', 'post_title')
    )
    ,'relate' => array(
      'scope' => 'fig-plot'
    )
));

piklist('field', array(
	'type' => 'datepicker'
	,'scope' => 'post_meta' // Not used for settings sections
	,'field' => 'fig_survey_date'
	,'label' => 'Date'
	,'required' => true
	,'description' => 'Date of the first date of the data collection event'
	,'attributes' => array(
		'class' => 'regular-text'
	)
	,'options' => array(
		'dateFormat' => 'yy-mm-dd'
		,'firstDay' => '0'
	)
	,'validate' => array(
		array(
			'type' => 'date'
		)
	)
));

piklist('field', array(
	'type' => 'select'
	,'scope' => 'post_meta'
	,'field' => 'fig-survey-weather'
	,'label' => 'Weather'
	,'choices' => array( '' => 'Choose Weather'  )
	    + piklist(get_terms('fig-survey-weather', array( 'hide_empty' => false))
	    ,array( 'term_id','name' )
	)
	,'attributes' => array(
		'class' => 'regular-text'
	)
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'fig_suvery_photo_north'
	,'scope' => 'post_meta'
	,'label' => __('Photo Journal - North','piklist')
	,'options' => array(
		'basic' => true
		,'button' => 'Add Photo'
	)
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'fig_suvery_photo_east'
	,'scope' => 'post_meta'
	,'label' => __('Photo Journal - East','piklist')
	,'options' => array(
		'basic' => true
		,'button' => 'Add Photo'
	)
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'fig_suvery_photo_south'
	,'scope' => 'post_meta'
	,'label' => __('Photo Journal - South','piklist')
	,'options' => array(
		'basic' => true
		,'button' => 'Add Photo'
	)
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'fig_suvery_photo_west'
	,'scope' => 'post_meta'
	,'label' => __('Photo Journal - West','piklist')
	,'options' => array(
		'basic' => true
		,'button' => 'Add Photo'
	)
));
