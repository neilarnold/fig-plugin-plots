<?php
/*
Title: Tree Information
Post Type: fig-survey
Context: normal
Priority: high
Order: 2
*/

wp_enqueue_script( 'math-js', plugin_dir_url( __FILE__ ) . '../../includes/js/math.min.js', array(), '2.5.0', true );
wp_enqueue_script( 'fig-form-js', plugin_dir_url( __FILE__ ) . '../../includes/js/back-end-form.js', array(), '2.5.0', true );

$softwood = get_terms('fig-survey-softwood', array( 'hide_empty' => false));
$hardwood = get_terms('fig-survey-hardwood', array( 'hide_empty' => false));
$fig_suvey_status = get_terms('fig-survey-status', array( 'hide_empty' => false));

piklist('field', array(
	'type' => 'select'
	,'scope' => 'post_meta'
	,'field' => 'fig_tree_softwood_calcs'
	,'choices' => piklist($softwood,array( 'term_id','description' ))
	,'attributes' => array(
		'wrapper_class' => 'container-hide',
		'class' => 'regular-text',
		'style' => 'display:none;'
	)
));

piklist('field', array(
	'type' => 'select'
	,'scope' => 'post_meta'
	,'field' => 'fig_tree_hardwood_calcs'
	,'choices' => piklist($hardwood, array( 'term_id','description' ))
	,'attributes' => array(
		'class' => 'regular-text',
		'style' => 'display:none;'
	)
));

piklist('field', array(
	'type' => 'group'
	,'scope' => 'post_meta'
	,'field' => 'fig_tree'
	,'label' => __('Tree')
	,'columns' => 12
	,'add_more' => true
	,'fields' => array(

		array(
			'type' => 'text'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_marker'
			,'columns' => 4
            ,'attributes' => array(
				'class' => 'regular-text',
				'placeholder' => 'Marker #'
            )
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta' // Not used for settings sections
			,'field' => 'fig_tree_type'
			,'columns' => 8
			//,'label' => 'Tree Type'
			//,'list' => false
			,'attributes' => array(
				'class' => 'text'
			)
			,'choices' => array(
				'' => 'Choose Tree Type'
				,'softwood' => 'Softwood'
				,'hardwood' => 'Hardwood'
			)
			//,'value' => 'softwood' // Sets default

		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_softwood'
			,'columns' => 12
			,'choices' => array( '' => 'Choose Softwood'  )
				+ piklist($softwood,array( 'term_id','name' ))
			,'attributes' => array(
				'class' => 'regular-text'
			)
			// ,'conditions' => array(
			// 	array(
			// 		'field' => 'fig_tree:fig_tree_type'
			// 		,'value' => 'softwood'
			// 	)
			// )
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_hardwood'
			,'columns' => 12
			,'choices' => array( '' => 'Choose Hardwood'  )
				+ piklist($hardwood,array( 'term_id','name' ))
			,'attributes' => array(
				'class' => 'regular-text'
			)
			// ,'conditions' => array(
			// 	array(
			// 		'field' => 'fig_tree:fig_tree_type'
			// 		,'value' => 'hardwood'
			// 	)
			// )
		)

		,array(
			'type' => 'select'
			//,'scope' => 'taxonomy'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_status'
			,'columns' => 12
			,'label' => 'Tree Status'
			,'choices' => array( '' => 'Choose Status'  )
				+ piklist($fig_suvey_status,array( 'term_id','name' ))
		)

		,array(
			'type' => 'text'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dbh'
			,'label' => 'DBH'
			,'Description' => 'in inches'
			,'columns' => 4
			,'attributes' => array(
				'placeholder' => 'DBH, in inches'
			)
			,'validate' => array(
				array(
					'type' => 'integer'
					,'options' => array( 'decimals' => 1 )
                )
				,array(
					'type' => 'range'
					,'options' => array(
						'min' => 1
						,'max' => 500
					)
				)
			)
		)

		,array(
			'type' => 'text'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_height_predicted'
			,'label' => 'Predicted Tree Height'
			,'Description' => 'in feet'
			,'columns' => 4
			,'attributes' => array(
				'placeholder' => 'Height, in feet'
				,'readonly' => 'readonly'
			)
		)

		,array(
			'type' => 'text'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_height'
			,'columns' => 4
			,'label' => 'Actual Tree Height'
			,'Description' => 'in feet'
			,'attributes' => array(
				'placeholder' => 'Height, in feet'
			)
			,'validate' => array(
				array(
					'type' => 'integer'
					,'options' => array( 'decimals' => 0 )
                )
				,array(
					'type' => 'range'
					,'options' => array(
						'min' => 1
						,'max' => 400
					)
				)
			)
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dmg_crown_type'
			,'label' => __('Damage Type Crown')
			,'columns' => 3
			,'attributes' => array( 'class' => 'regular-text' )
			,'choices' => array(
				'' => ''
				,'none' => 'None'
				,'branches' => 'Branches'
				,'foliage' => 'Foliage'
				,'both' => 'Both'
			)
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dmg_crown_pct'
			,'label' => __('Damage % Crown')
			,'columns' => 3
			,'attributes' => array( 'class' => 'regular-text' )
			,'choices' => array(
				'' => ''
				,'0-25' => '0-25%'
				,'25-50' => '25-50%'
				,'51-75' => '51-75%'
				,'76-100' => '76-100%'
			)
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dmg_bole_type'
			,'label' => __('Damage Type Bole')
			,'columns' => 3
			,'attributes' => array( 'class' => 'regular-text' )
			,'choices' => array(
				'' => ''
				,'none' => 'None'
				,'insects' => 'Insects'
				,'disease' => 'Disease'
				,'mechanical' => 'Mechanical'
				,'weather' => 'Weather'
				,'all' => 'All'
				,'other' => 'Other'
			)
		)

		,array(
			'type' => 'select'
			,'scope' => 'post_meta'
			,'field' => 'fig_tree_dmg_bole_pct'
			,'label' => __('Damage % Bole')
			,'columns' => 3
			,'attributes' => array( 'class' => 'regular-text' )
			,'choices' => array(
				'' => ''
				,'0-25' => '0-25%'
				,'25-50' => '25-50%'
				,'51-75' => '51-75%'
				,'76-100' => '76-100%'
			)
		)


	)
));
