<?php
/*
Title: Plot Information 
Post Type: fig-plot
Context: normal
Priority: high
Order: 1
*/

piklist('field', array(
	'type' => 'select'
	,'scope' => 'post_meta' // Not used for settings sections
	,'field' => 'fig_plot_bioregion'
	,'label' => __('Bioregion')
	,'attributes' => array(
		'class' => 'regular-text'
	)
	,'choices' => array(
		'Aroostook hills' => 'Aroostook hills'
		,'Aroostook lowlands' => 'Aroostook lowlands'
		,'Casco Bay coast' => 'Casco Bay coast'
		,'Central Maine embayment' => 'Central Maine embayment'
		,'Central Maine foothills' => 'Central Maine foothills'
		,'Connecticut lakes' => 'Connecticut lakes'
		,'Gulf of Maine coastal lowland' => 'Gulf of Maine coastal lowland'
		,'Gulf of Maine coastal plain' => 'Gulf of Maine coastal plain'
		,'International boundary plateau' => 'International boundary plateau'
		,'Mahoosic Rangeley Lakes' => 'Mahoosic Rangeley Lakes'
		,'Maine central mountains' => 'Maine central mountains'
		,'Maine eastern coastal' => 'Maine eastern coastal'
		,'Maine eastern interior' => 'Maine eastern interior'
		,'Maine-New Brunswick lowlands' => 'Maine-New Brunswick lowlands'
		,'Penobscot coast' => 'Penobscot coast'
		,'Sebago-Ossipee hills & plains' => 'Sebago-Ossipee hills & plains'
		,'St. John upland' => 'St. John upland'
		,'Western Maine foothills' => 'Western Maine foothills'
		,'White Mountains' => 'White Mountains'


	)
));

piklist('field', array(
    'type' => 'text'
    ,'scope' => 'post_meta' // Not used for settings sections
    ,'field' => 'fig_plot_location'
    ,'label' => __('Town/City')
    ,'attributes' => array(
        'class' => 'regular-text', 
        'placeholder' => 'ex: Portland, ME'
    )
	,'sanitize' => array(
		array(
			'type' => 'text_field'
			)
    )
));

piklist('field', array(
	'type' => 'group'
	,'field' => 'fig_plot_longlat'
	,'label' => __('Location') 
	,'columns' => 12
	,'add_more' => false
	,'fields' => array(
		array(
			'type' => 'text'
			,'scope' => 'post_meta' // Not used for settings sections
			,'field' => 'fig_plot_latitude'
			,'label' => __('Latitude')
			,'columns' => 6
			,'attributes' => array(
				'class' => 'regular-text', 
				'placeholder' => 'ex: 43.651678'
			)
			,'validate' => array(
				array(
					'type' => 'range'
					,'options' => array(
						'min' => -90
						,'max' => 90
					)
				)
			)
		),
		array(
			'type' => 'text'
			,'scope' => 'post_meta' // Not used for settings sections
			,'field' => 'fig_plot_longitude'
			,'label' => __('Longitude')
			,'columns' => 6
			,'attributes' => array(
				'class' => 'regular-text', 
				'placeholder' => 'ex: -70.256359'
			)
			,'validate' => array(
				array(
					'type' => 'range'
					,'options' => array(
						'min' => -180
						,'max' => 180
					)
				)
			)
		)
	)
));



piklist('field', array(
    'type' => 'text'
    ,'scope' => 'post_meta' // Not used for settings sections
    ,'field' => 'fig_plot_organization'
    ,'label' => __('School/Organization')
    ,'attributes' => array(
        'class' => 'regular-text', 
        'placeholder' => 'ex: Forest High School'
    )
	,'sanitize' => array(
		array(
			'type' => 'text_field'
			)
    )
));

piklist('field', array(
    'type' => 'text'
    ,'scope' => 'post_meta' // Not used for settings sections
    ,'field' => 'fig_plot_forestry_partner'
    ,'label' => __('Forestry Partner')
    ,'attributes' => array(
        'class' => 'regular-text', 
        'placeholder' => 'ex: ??'
    )
	,'sanitize' => array(
		array(
			'type' => 'text_field'
			)
    )
));

piklist('field', array(
    'type' => 'text'
    ,'scope' => 'post_meta' // Not used for settings sections
    ,'field' => 'fig_plot_teacher'
    ,'label' => __('Teacher')
    ,'attributes' => array(
        'class' => 'regular-text', 
        'placeholder' => 'ex: Mrs. Smith'
    )
	,'sanitize' => array(
		array(
			'type' => 'text_field'
			)
    )
));