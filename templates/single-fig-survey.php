<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Fig Survey Template
 *
Template Name:  Fig Survey Page
 *
 * @file           single-fig-survey.php
 * @author         Neil Arnold
 * @copyright      2015 Neil Arnold
 * @version        Release: 1.0
 * @filesource     wp-content/plugins/fig-plots/templates/single-fig-survey.php
 */


 get_header(); ?>
 
<div class="x-container max width offset">
	<div class="<?php x_main_content_class(); ?>" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

        <?php the_content(); ?>

	<?php endwhile; ?>
	</div>

	<aside class="<?php x_sidebar_class(); ?>" role="complementary">
		<?php dynamic_sidebar( 'plots-surveys' ); ?>
	</aide>

  </div>

<?php get_footer(); ?>