<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Fig Plot Template
 *
Template Name:  Fig Plot Page
 *
 * @file           single-fig-plot.php
 * @author         Neil Arnold
 * @copyright      2015 Neil Arnold
 * @version        Release: 1.0
 * @filesource     wp-content/plugins/fig-plots/templates/single-fig-plot.php
 */

$utils = new FigUtils;
$fig_plot = new FigPlot;

$fig_plot_bioregion = get_post_meta($post->ID, 'fig_plot_bioregion', true);
$fig_plot_location = get_post_meta($post->ID, 'fig_plot_location', true);
$fig_plot_longlat = get_post_meta($post->ID, 'fig_plot_longlat', true);
$fig_plot_organization = get_post_meta($post->ID, 'fig_plot_organization', true);
$fig_plot_forestry_partner = get_post_meta($post->ID, 'fig_plot_forestry_partner', true);
$fig_plot_teacher = get_post_meta($post->ID, 'fig_plot_teacher', true);

 get_header(); ?>

<div class="x-container max width offset">
	<div class="<?php x_main_content_class(); ?>" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

		<div class="x-column x-sm x-1-2">
			<h3>Plot Info</h3>
            <div class="plot-info">
			    <?=FigUtils::ShowWithLabel("Bioregion", $fig_plot_bioregion);?>
			    <?=FigUtils::ShowWithLabel("Town", $fig_plot_location);?>
			    <?=FigUtils::ShowLongLat("Latitude & Longitude", $fig_plot_longlat);?>
			    <?=FigUtils::ShowWithLabel("School or Organization", $fig_plot_organization);?>
			    <?=FigUtils::ShowWithLabel("Forestry Partner", $fig_plot_forestry_partner);?>
			    <?=FigUtils::ShowWithLabel("Teacher", $fig_plot_teacher);?>
            </div>

			<h3>Survey Data</h3>
			<?php
			//$query_years = $fig_plot->GetDataByYear($post->ID, 2015);
			$available_years = $fig_plot->GetYearsOfData($post->ID);
			//$query_years = $fig_plot->GetDataByYear($post->ID);

			if (count($available_years) > 0 ) {
				echo '<ul class="all-fig-plots">';
				foreach($available_years as $year) {
					if ( is_user_logged_in() )
    					echo '<li><a href="./' . $year . '/">' . $year . ': view data table</a></li>';
					else
						echo '<li>' . $year . '</li>';
				}
				echo '</ul>';
				if ( !is_user_logged_in() )
					echo '<p><a href="/wp-admin">Log in</a> to see the survey data for this plot. If you don’t have an account, contact <a href="/contact-us">Pat Maloney</a>, to request one.</p>';
			} else {
				echo '<p><b>There are no surveys for this Plot.</b></p>';
			}
			wp_reset_query();
			?>

		</div>

		<div class="x-column x-sm x-1-2 last">
			<h3>Photo Journal</h3>

			<?php
			$picture_posts = $fig_plot->GetDataByPlot($post->ID);

			if ( $picture_posts->have_posts() ) {
				echo '<table class="table-picture-post">';
				echo '<thead><tr>';
				echo '<th>Date</th><th>N</th><th>E</th><th>S</th><th>W</th>';
				echo '</tr></thead><tbody>';
				while ( $picture_posts->have_posts() ) {
					$picture_posts->the_post();

					$survey_date = get_post_meta($post->ID, 'fig_survey_date', true);

					$picture_north = get_post_meta($picture_posts->post->ID, 'fig_suvery_photo_north', true);
					$picture_east = get_post_meta($picture_posts->post->ID, 'fig_suvery_photo_east', true);
					$picture_south = get_post_meta($picture_posts->post->ID, 'fig_suvery_photo_south', true);
					$picture_west = get_post_meta($picture_posts->post->ID, 'fig_suvery_photo_west', true);

					$row = '';
					if (!empty($picture_north) || !empty($picture_east) || !empty($picture_south) || !empty($picture_west)) {
						$row = '<tr>';
						$row .= '<td>' . date("n/Y", strtotime($survey_date)) . '</td>';
						$row .= '<td>' . FigUtils::ShowImage($picture_north) . '</td>';
						$row .= '<td>' . FigUtils::ShowImage($picture_east) . '</td>';
						$row .= '<td>' . FigUtils::ShowImage($picture_south) . '</td>';
						$row .= '<td>' . FigUtils::ShowImage($picture_west) . '</td>';
						$row .= '</tr>';
					}
					echo $row;

				}
				echo '</tbody></table>';
				echo '<div class="detail-picture-post"><img /></div>';
			} else {
				echo "<p><b>There are no surveys for this Plot.</b></p>";
			}
			wp_reset_query();
			?>

		</div>

		<hr class="x-clear">

		<?php
		if (comments_open()) {
			echo '<div class="x-column x-sm x-1-1">';
			//echo '<h3>Comments</h3>';
            //echo comments_number();
			comments_template( '', true );
			echo '</div><hr class="x-clear">';
		}
		?>



	</div>

	<aside class="<?php x_sidebar_class(); ?>" role="complementary">
		<?php dynamic_sidebar( 'plots-surveys' ); ?>
	</aide>

  </div>

<?php get_footer(); ?>
