<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( !is_user_logged_in() ) {
	wp_redirect( get_permalink( 10 ));
	exit();
}

setlocale(LC_MONETARY, 'en_US');
define('FIG_PAGE', true);
/**
 * Survey Year Template
 * Template Name:  Survey by Year Page
 *
 * @file           single-fig-survey-year.php
 * @author         Neil Arnold
 * @copyright      2015 Neil Arnold
 * @version        Release: 1.0
 * @filesource     wp-content/plugins/fig-plots/templates/single-fig-survey-year.php
 */

global $post;

$plot_slug = $wp_query->query_vars['fig_plot'];
$survey_year = $wp_query->query_vars['fig_year'];

$post = FigPlot::GetPlotPost($plot_slug);
//$plot_title = $post->post_title;

$fig_plot = new FigPlot;

$csv = '';
$html = '';
$current_user_id = get_current_user_id();

$surveys = $fig_plot->GetDataByYear($post->ID, $survey_year);

if ( $surveys->have_posts() ) {

	// Table of Survey & Tree Data
	$table_config = array(
		array("Tree&nbsp;#", "fig_tree_marker", ""),
		array("Species", "fig_tree_type", "species"),
		array("Status", "fig_tree_status", "category:fig-survey-status"),
		array("Predicted Height", "fig_tree_height_predicted", "sprintf:%g'"),
		array("Actual Height", "fig_tree_height", "sprintf:%g'"),
		array("DBH", "fig_tree_dbh", "sprintf:%g\""),
		array("Damage Type Crown", "fig_tree_dmg_crown_type", "ucwords"),
		array("Damage % Crown", "fig_tree_dmg_crown_pct", ""),
		array("Damage Type Bole", "fig_tree_dmg_bole_type", "ucwords"),
		array("Damage % Bole", "fig_tree_dmg_bole_pct", ""),
	);

	$html .= '<table class="table-survey-by-year responsive">';

	$html .= '<thead><tr>';
	foreach ($table_config as $row) {
		$html .= '<th>' . $row[0] . '</th>';
		$csv .= '"' . $row[0] . '",';
	}
	$html .= '</tr></thead>';
	$csv .= '""' . PHP_EOL;

	$html .= '<tbody>';
	while ( $surveys->have_posts() ) {
		$surveys->the_post();
		$survey_user_id = $post->post_author;

		$survey_date = get_post_meta($post->ID, 'fig_survey_date', true);
		$survey_date = date_format(date_create( $survey_date), "M d, Y");

		$html .= '<tr><td class="survey-date" colspan="2">' . $survey_date . '</td><td class="survey-date" colspan="9">&nbsp;</td></tr>';
		$csv .= '"' . $survey_date . '"' . PHP_EOL;

		$trees = get_post_meta($surveys->post->ID, 'fig_tree', true);



		foreach ($trees as $tree) {
			$html .= '<tr>';

			foreach ($table_config as $row) {
				$val = $tree[$row[1]];
				if ($row[2] != "") {
					$formatter = split(':', $row[2]);
					if ($formatter[0] == "category")
						$val = get_term( $val, $formatter[1])->name;
					else if ($formatter[0] == "species") {
						//$val = $val[0];
						//echo $val[0] . ', ';
						$val = get_term( $tree['fig_tree_' . $val], 'fig-survey-' . $val)->name;
					} else if ($formatter[0] == "ucwords")
						$val = ucwords($val);
					else if ($formatter[0] == "sprintf") {
						//$val = sprintf($formatter[1], $val);
					}
				}
				$html .= '<td>' . $val . '</td>';
				$csv .= '"' . str_replace('"', '""', $val) . '",';

			}
			$html .= '<td>'. ($current_user_id == $survey_user_id ? '<a href="/enter-your-fig-data//?_post[ID]=' . $surveys->post->ID . '"><i class="fa fa-pencil-square-o"></i></a>' : '') . '</td>';
			$html .= '</tr>';
			$csv .= '""' . PHP_EOL;
		}
	}
	//wp_reset_postdata();
	$html .= '</tbody>';

    //$html .= '<tfoot><tr><td colspan="10"></td></tr></tfoot>';
	$html .= '</table>';
	$html .= '<a href="?download=true" class="button">Download this Dataset</a>';
} else {
	$html .= '<p>No Surveys Found for ' . $survey_year . '</p>';
}

if ($_GET["download"] == "true")
	download_csv('data_' . $survey_year . '.csv', $csv);


get_header();


?>

<div class="x-container max width offset">
	<div class="<?php x_main_content_class(); ?>" role="main">

		<h4 class="h-widget">Plot <?=$post->post_title?>, All Surveys for <?=$survey_year?></h4>

		<?=$html?>


	</div>

	<aside class="<?php x_sidebar_class(); ?>" role="complementary">
		<?php dynamic_sidebar( 'plots-surveys' ); ?>
	</aide>

  </div>

<?php get_footer();

function download_csv($filename, $csv) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
	echo $csv;
	die;
}
?>
