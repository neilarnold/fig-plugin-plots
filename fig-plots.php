<?php
/*
Plugin Name: FIG Plots & Trees
Plugin URI: http://www.neilarnold.com
Description: Custom Plugin to manage FIG plots and tree growth data.
Version: 0.9
Author: Neil Arnold
Author URI: http://www.neilarnold.com
Plugin Type: Piklist
License: GPL2
*/


// Construct
function __construct() {
          register_activation_hook(__FILE__,array($this,'activate'));
          add_action( 'init', array( $this, 'create_taxonomies' ) );
     } 

// Check for Piklist --- --- --- 
function checkForPiklist() {
    if(is_admin())   {
        include_once('includes/class-piklist-checker.php');
        if (!piklist_checker::check(__FILE__))
            return;
    }
}
add_action('init', 'checkForPiklist');
// END - Check for Piklist --- --- ---


// Register Custom Post Type --- --- ---
function fig_init_cpt($post_types)
{
	$post_types['fig-plot'] = array(
		'labels' => piklist('post_type_labels', 'FIG Plot')
		,'title' => __('Enter a new Plot Number')
		,'public' => true
        ,'rewrite' => array( 'slug' => 'plot' )
		,'supports' => array( 'title', 'author', 'thumbnail', 'revisions', 'comments' )
        ,'hide_meta_box' => array( 'slug' , 'editor')
        ,'menu_icon' => 'dashicons-location-alt'
        ,'has_archive' => false
		,'edit_columns' => array(
			'title' => __('Plot #')
			,'author' => __('Assigned to')
		)
	);

	$post_types['fig-survey'] = array(
		'labels' => piklist('post_type_labels', 'FIG Survey')
		,'title' => __('Enter a new Survey')
		,'public' => true
        ,'rewrite' => array( 'slug' => 'survey' )
		,'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'comments' )
        ,'hide_meta_box' => array( 'slug' , 'comments' , 'commentstatus' )
        ,'menu_icon' => 'dashicons-palmtree'
        ,'has_archive' => false
	);

	return $post_types;
}
add_filter('piklist_post_types', 'fig_init_cpt');
// END - Register Custom Post Type --- --- ---


// Register Custom Taxonomies --- --- ---
function fig_init_taxonomies($taxonomies)
{
	$taxonomies[] = array(
      'post_type' => 'fig-survey'
      ,'name' => 'fig-survey-softwood'
      ,'configuration' => array(
        'hierarchical' => false
        ,'labels' => piklist('taxonomy_labels', 'Softwood')
        ,'page_icon' => 'dashicons-palmtree'
        ,'show_ui' => true
        ,'query_var' => true
        ,'rewrite' => array(
          'slug' => 'fig-survey-softwood'
        )
        ,'show_admin_column' => false
        ,'list_table_filter' => false
        ,'meta_box_filter' => true
        ,'comments' => false
      )
    );

	$taxonomies[] = array(
      'post_type' => 'fig-survey'
      ,'name' => 'fig-survey-hardwood'
      ,'configuration' => array(
        'hierarchical' => false
        ,'labels' => piklist('taxonomy_labels', 'Hardwood')
        ,'page_icon' => 'dashicons-palmtree'
        ,'show_ui' => true
        ,'query_var' => true
        ,'rewrite' => array(
          'slug' => 'fig-survey-hardwood'
        )
        ,'show_admin_column' => false
        ,'list_table_filter' => false
        ,'meta_box_filter' => true
        ,'comments' => false
      )
    );

	$taxonomies[] = array(
      'post_type' => 'fig-survey'
      ,'name' => 'fig-survey-weather'
      ,'configuration' => array(
        'hierarchical' => false
        ,'labels' => piklist('taxonomy_labels', 'Weather')
        ,'page_icon' => 'dashicons-cloud'
        ,'show_ui' => true
        ,'query_var' => true
        ,'rewrite' => array(
          'slug' => 'fig-survey-weather'
        )
        ,'show_admin_column' => false
        ,'list_table_filter' => false
        ,'meta_box_filter' => true
        ,'comments' => false
      )
    );

	$taxonomies[] = array(
      'post_type' => 'fig-survey'
      ,'name' => 'fig-survey-status'
      ,'configuration' => array(
        'hierarchical' => false
        ,'labels' => piklist('taxonomy_labels', 'Tree Statuses')
        ,'page_icon' => 'dashicons-carrot'
        ,'show_ui' => true
        ,'query_var' => true
        ,'rewrite' => array(
          'slug' => 'fig-survey-status'
        )
        ,'show_admin_column' => false
        ,'list_table_filter' => false
        ,'meta_box_filter' => true
        ,'comments' => false
      )
    );

    return $taxonomies;
}
add_filter('piklist_taxonomies', 'fig_init_taxonomies');

// Preload Some Taxonomy Terms --- --- ---
function fig_tax_insert_terms () {
	if ( !term_exists( 'Live', 'fig-survey-status' ) ) 
		wp_insert_term ( 'Live', 'fig-survey-status');
	if ( !term_exists( 'Dead Standing', 'fig-survey-status' ) ) 
		wp_insert_term ( 'Dead Standing', 'fig-survey-status');
	if ( !term_exists( 'Dead Down', 'fig-survey-status' ) ) 
		wp_insert_term ( 'Dead Down', 'fig-survey-status');
	if ( !term_exists( 'Dead Harvested', 'fig-survey-status' ) ) 
		wp_insert_term ( 'Dead Harvested', 'fig-survey-status');

		
	if ( !term_exists( 'Sunny', 'fig-survey-weather' ) ) 
		wp_insert_term ( 'Sunny', 'fig-survey-weather');
	if ( !term_exists( 'Partly Cloudy', 'fig-survey-weather' ) ) 
		wp_insert_term ( 'Partly Cloudy', 'fig-survey-weather');
	if ( !term_exists( 'Cloudy', 'fig-survey-weather' ) ) 
		wp_insert_term ( 'Cloudy', 'fig-survey-weather');
	if ( !term_exists( 'Rain', 'fig-survey-weather' ) ) 
		wp_insert_term ( 'Rain', 'fig-survey-weather');
	if ( !term_exists( 'Snow', 'fig-survey-weather' ) ) 
		wp_insert_term ( 'Snow', 'fig-survey-weather');

	// Softwoods - check first item, add all.
	if ( !term_exists( 'Atlantic White Cedar', 'fig-survey-softwood' ) ) {
		wp_insert_term( 'Atlantic White Cedar', 'fig-survey-softwood', array( 'description' => '(89.3969*1)*(1-EXP(-0.0969*DBH))^((0.0124*30)+(0.315*2)+(0.01)^1.85)' ) );
		wp_insert_term( 'Atlantic White Cedar', 'fig-survey-softwood', array( 'description' => '(89.3969*1)*(1-EXP(-0.0969*DBH))^((0.0124*30)+(0.315*2)+(0.01)^1.85)' ) );
		wp_insert_term( 'Balsam Fir', 'fig-survey-softwood', array( 'description' => '(81.1055*1)*(1-EXP(-0.1159*DBH))^((0.0089*30)+(0.3524*2)+(0.01)^2.2889)' ) );
		wp_insert_term( 'Black Spruce', 'fig-survey-softwood', array( 'description' => '(82.3788*1)*(1-EXP(-0.1076*DBH))^((0.0119*30)+(0.3012*2)+(0.01)^2.5734)' ) );
		wp_insert_term( 'Eastern Hemlock', 'fig-survey-softwood', array( 'description' => '(80.928*1)*(1-EXP(-0.0947*DBH))^((0.0085*30)+(0.3092*2)+(0.01)^2.0523)' ) );
		wp_insert_term( 'Eastern Red Cedar', 'fig-survey-softwood', array( 'description' => '(89.3969*1)*(1-EXP(-0.0969*DBH))^((0.0124*30)+(0.315*2)+(0.01)^1.85)' ) );
		wp_insert_term( 'Eastern White Pine', 'fig-survey-softwood', array( 'description' => '(102.9998*1)*(1-EXP(-0.0762*DBH))^((0.0126*30)+(0.2908*2)+(0.01)^2.4458)' ) );
		wp_insert_term( 'Introduced Larch', 'fig-survey-softwood', array( 'description' => '(92.5925*1)*(1-EXP(-0.1161*DBH))^((0.0116*30)+(0.2785*2)+(0.01)^2.2087)' ) );
		wp_insert_term( 'Jack Pine', 'fig-survey-softwood', array( 'description' => '(89.3969*1)*(1-EXP(-0.0969*DBH))^((0.0124*30)+(0.315*2)+(0.01)^1.85)' ) );
		wp_insert_term( 'Larch spp.', 'fig-survey-softwood', array( 'description' => '(92.5925*1)*(1-EXP(-0.1161*DBH))^((0.0116*30)+(0.2785*2)+(0.01)^2.2087)' ) );
		wp_insert_term( 'Misc. Softwood spp.', 'fig-survey-softwood', array( 'description' => '(89.3969*1)*(1-EXP(-0.0969*DBH))^((0.0124*30)+(0.315*2)+(0.01)^1.85)' ) );
		wp_insert_term( 'Northern White Cedar', 'fig-survey-softwood', array( 'description' => '(68.0198*1)*(1-EXP(-0.1014*DBH))^((0.0069*30)+(0.3495*2)+(0.01)^2.4178)' ) );
		wp_insert_term( 'Norway Spruce', 'fig-survey-softwood', array( 'description' => '(82.3788*1)*(1-EXP(-0.1076*DBH))^((0.0119*30)+(0.3012*2)+(0.01)^2.5734)' ) );
		wp_insert_term( 'Pitch Pine', 'fig-survey-softwood', array( 'description' => '(89.3969*1)*(1-EXP(-0.0969*DBH))^((0.0124*30)+(0.315*2)+(0.01)^1.85)' ) );
		wp_insert_term( 'Red Pine', 'fig-survey-softwood', array( 'description' => '(89.3969*1)*(1-EXP(-0.0969*DBH))^((0.0124*30)+(0.315*2)+(0.01)^1.85)' ) );
		wp_insert_term( 'Red Spruce', 'fig-survey-softwood', array( 'description' => '(82.3788*1)*(1-EXP(-0.1076*DBH))^((0.0119*30)+(0.3012*2)+(0.01)^2.5734)' ) );
		wp_insert_term( 'Scotch Pine', 'fig-survey-softwood', array( 'description' => '(89.3969*1)*(1-EXP(-0.0969*DBH))^((0.0124*30)+(0.315*2)+(0.01)^1.85)' ) );
		wp_insert_term( 'Spruce spp.', 'fig-survey-softwood', array( 'description' => '(82.3788*1)*(1-EXP(-0.1076*DBH))^((0.0119*30)+(0.3012*2)+(0.01)^2.5734)' ) );
		wp_insert_term( 'Tamarack', 'fig-survey-softwood', array( 'description' => '(92.5925*1)*(1-EXP(-0.1161*DBH))^((0.0116*30)+(0.2785*2)+(0.01)^2.2087)' ) );
		wp_insert_term( 'White Spruce', 'fig-survey-softwood', array( 'description' => '(82.3788*1)*(1-EXP(-0.1076*DBH))^((0.0119*30)+(0.3012*2)+(0.01)^2.5734)' ) );
	}
	if ( !term_exists( 'American Basswood', 'fig-survey-hardwood' ) ) {
		wp_insert_term( 'American Basswood', 'fig-survey-hardwood', array( 'description' => '(91.519*1)*(1-EXP(-0.081*DBH))^((0.0022*30)+(0.056*2)+(0.01)^0.1471)' ) );
		wp_insert_term( 'American Beech', 'fig-survey-hardwood', array( 'description' => '(80.7061*1)*(1-EXP(-0.0811*DBH))^((0.0017*30)+(0.0796*2)+(0.01)^0.1738)' ) );
		wp_insert_term( 'American Chestnut', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'American Elm', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'American Hornbeam', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'American Mountain-Ash', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Apple spp.', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Ash spp.', 'fig-survey-hardwood', array( 'description' => '(88.7389*1)*(1-EXP(-0.1158*DBH))^((0.0034*30)+(0.2252*2)+(0.01)^0.2315)' ) );
		wp_insert_term( 'Aspen spp.', 'fig-survey-hardwood', array( 'description' => '(91.5048*1)*(1-EXP(-0.1023*DBH))^((0.0054*30)+(0.0638*2)+(0.01)^0.1422)' ) );
		wp_insert_term( 'Balsam Popular', 'fig-survey-hardwood', array( 'description' => '(91.5048*1)*(1-EXP(-0.1023*DBH))^((0.0054*30)+(0.0638*2)+(0.01)^0.1422)' ) );
		wp_insert_term( 'Basswood spp.', 'fig-survey-hardwood', array( 'description' => '(91.519*1)*(1-EXP(-0.081*DBH))^((0.0022*30)+(0.056*2)+(0.01)^0.1471)' ) );
		wp_insert_term( 'Bear Oak', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Bigtooth Aspen', 'fig-survey-hardwood', array( 'description' => '(91.5048*1)*(1-EXP(-0.1023*DBH))^((0.0054*30)+(0.0638*2)+(0.01)^0.1422)' ) );
		wp_insert_term( 'Black Ash', 'fig-survey-hardwood', array( 'description' => '(88.7389*1)*(1-EXP(-0.1158*DBH))^((0.0034*30)+(0.2252*2)+(0.01)^0.2315)' ) );
		wp_insert_term( 'Black Cherry', 'fig-survey-hardwood', array( 'description' => '(91.519*1)*(1-EXP(-0.081*DBH))^((0.0022*30)+(0.056*2)+(0.01)^0.1471)' ) );
		wp_insert_term( 'Black Locust', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Black Oak', 'fig-survey-hardwood', array( 'description' => '(91.519*1)*(1-EXP(-0.081*DBH))^((0.0022*30)+(0.056*2)+(0.01)^0.1471)' ) );
		wp_insert_term( 'Black Tupelo', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Black Willow', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Boxelder', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Bur Oak', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Butternut', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Chestnut Oak', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Common Chokecherry', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Eastern Cottonwood', 'fig-survey-hardwood', array( 'description' => '(91.5048*1)*(1-EXP(-0.1023*DBH))^((0.0054*30)+(0.0638*2)+(0.01)^0.1422)' ) );
		wp_insert_term( 'Eastern Hophornbeam', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Elm spp.', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Gray Birch', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Green Ash', 'fig-survey-hardwood', array( 'description' => '(88.7389*1)*(1-EXP(-0.1158*DBH))^((0.0034*30)+(0.2252*2)+(0.01)^0.2315)' ) );
		wp_insert_term( 'Maple spp.', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Misc. Hardwood spp.', 'fig-survey-hardwood', array( 'description' => '(91.519*1)*(1-EXP(-0.081*DBH))^((0.0022*30)+(0.056*2)+(0.01)^0.1471)' ) );
		wp_insert_term( 'Mountain Maple', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Northern Red Oak', 'fig-survey-hardwood', array( 'description' => '(91.519*1)*(1-EXP(-0.081*DBH))^((0.0022*30)+(0.056*2)+(0.01)^0.1471)' ) );
		wp_insert_term( 'Norway Maple', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Ohio Buckeye', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Osage Orange', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Other Hardwood spp.', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Paper Birch', 'fig-survey-hardwood', array( 'description' => '(82.3602*1)*(1-EXP(-0.1075*DBH))^((0.0055*30)+(0.1*2)+(0.01)^0.1828)' ) );
		wp_insert_term( 'Pin Cherry', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Quaking Aspen', 'fig-survey-hardwood', array( 'description' => '(91.5048*1)*(1-EXP(-0.1023*DBH))^((0.0054*30)+(0.0638*2)+(0.01)^0.1422)' ) );
		wp_insert_term( 'Red Maple', 'fig-survey-hardwood', array( 'description' => '(80.8032*1)*(1-EXP(-0.1015*DBH))^((0.0033*30)+(0.109*2)+(0.01)^0.2142)' ) );
		wp_insert_term( 'Sassafras', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Scarlet Oak', 'fig-survey-hardwood', array( 'description' => '(91.519*1)*(1-EXP(-0.081*DBH))^((0.0022*30)+(0.056*2)+(0.01)^0.1471)' ) );
		wp_insert_term( 'Serviceberry spp.', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Shagbark Hickory', 'fig-survey-hardwood', array( 'description' => '(91.519*1)*(1-EXP(-0.081*DBH))^((0.0022*30)+(0.056*2)+(0.01)^0.1471)' ) );
		wp_insert_term( 'Silver Maple', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Striped Maple', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Sugar Maple', 'fig-survey-hardwood', array( 'description' => '(81.4198*1)*(1-EXP(-0.0904*DBH))^((0.0025*30)+(0.0377*2)+(0.01)^0.1791)' ) );
		wp_insert_term( 'Swamp Cottonwood', 'fig-survey-hardwood', array( 'description' => '(91.5048*1)*(1-EXP(-0.1023*DBH))^((0.0054*30)+(0.0638*2)+(0.01)^0.1422)' ) );
		wp_insert_term( 'Swamp White Oak', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Sweet Birch', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'White Ash', 'fig-survey-hardwood', array( 'description' => '(88.7389*1)*(1-EXP(-0.1158*DBH))^((0.0034*30)+(0.2252*2)+(0.01)^0.2315)' ) );
		wp_insert_term( 'White Oak', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'White Willow', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Willow spp.', 'fig-survey-hardwood', array( 'description' => '(87.7027*1)*(1-EXP(-0.0762*DBH))^((0.0025*30)+(0.1215*2)+(0.01)^0.1929)' ) );
		wp_insert_term( 'Yellow Birch', 'fig-survey-hardwood', array( 'description' => '(74.5607*1)*(1-EXP(-0.0831*DBH))^((0.0028*30)+(0.0465*2)+(0.01)^0.2132)' ) );
	}
  
}
add_action( 'init', 'fig_tax_insert_terms', 99);
// END - Register Custom Taxonomies --- --- ---


// Remove Revolution Slider from Custom Post Type --- --- ---
function remove_revolution_slider_meta_boxes() {
	remove_meta_box( 'mymetabox_revslider_0', 'fig-plot', 'normal' );
	remove_meta_box( 'mymetabox_revslider_0', 'fig-survey', 'normal' );
}
add_action( 'do_meta_boxes', 'remove_revolution_slider_meta_boxes' );
// END - Remove Revolution Slider from Custom Post Type --- --- ---


// Register Custom Template --- --- ---
function fig_suvey_single_template($single_template) {
     global $post;
     
	if ($post->post_type == 'fig-survey') {
          $single_template = plugin_dir_path(__FILE__ ).'templates/single-fig-survey.php';
     }
	 if ($post->post_type == 'fig-plot') {
          $single_template = plugin_dir_path(__FILE__ ).'templates/single-fig-plot.php';
     }

     return $single_template;
}
add_filter( "single_template", "fig_suvey_single_template" );
// END - Register Custom Template --- --- ---


// Create new custom page that shows a certain year of surveys --- --- ---
function fig_handle_custompage_route()
{
    add_rewrite_rule('plot/([^/]+)/([^/]+)/?', 'index.php?fig_plot=$matches[1]&fig_year=$matches[2]', 'top');
    flush_rewrite_rules();
}
add_action('admin_init', 'fig_handle_custompage_route');

function fig_declare_vars()
{
    add_rewrite_tag('%fig_plot%', '([^&]+)');
    add_rewrite_tag('%fig_year%', '([^&]+)');
}
add_filter('init', 'fig_declare_vars');


function fig_survey_year_template($template)
{
    global $wp_query;
    if (isset($wp_query->query_vars['fig_year'])) {
        return dirname(__FILE__) . '/templates/single-fig-survey-year.php';
    }
    return $template;
}
add_filter('template_include', 'fig_survey_year_template', 1, 1);

function rewrite_if_fig_plot( $title, $id = null ) {
    global $wp_query; 
    if (isset($wp_query->query_vars['fig_year'])) {
		$plot_slug = $wp_query->query_vars['fig_plot'];
		$plot = FigPlot::GetPlotPost($plot_slug);
		$plot_title = $plot->post_title;
		echo $plot_title . ' - ';	// newest x-theme needs it's echo'd, not returned.
		return $plot_title;
    }
	
    return $title;
}
add_filter( 'wp_title', 'rewrite_if_fig_plot', 10, 2 );

// END - Create new custom page that shows a certain year of surveys --- --- ---


// Include CSS & JS Where Appropriate --- --- --- 
function fig_admin_head(){
	wp_enqueue_style( 'fig-admin-css', plugin_dir_url( __FILE__ ) . 'includes/css/back-end.css' );
	wp_enqueue_script( 'math-js', plugin_dir_url( __FILE__ ) . 'includes/js/math.min.js', array(), '2.5.0', true );
}
add_action('admin_head', 'fig_admin_head');
// END - Include CSS & JS Where Appropriate --- --- --- 


// Include JS Library in Template --- --- --- 
function fig_in_wp_head($pid){
	global $wp_query;    
	
	$qv_fig_year = '';
	if (qv_isset("fig_year"))
		$qv_fig_year = $wp_query->query_vars['fig_year'];

	if (is_single() || $wp_query->query_vars['fig_year'] != '') {
        global $post;
		if ($post->post_type ==  'fig-plot' || $post->post_type ==  'fig-survey') { 
            wp_enqueue_style( 'fig-files', plugin_dir_url( __FILE__ ) . 'includes/css/front-end.css' );
            wp_enqueue_script( 'fig-js', plugin_dir_url( __FILE__ ) . 'includes/js/front-end.js', array(), '3.1.0', true );
			wp_enqueue_script( 'math-js', plugin_dir_url( __FILE__ ) . 'includes/js/math.min.js', array(), '2.5.0', true );
        }
		if ($wp_query->query_vars['fig_year'] != '') {
			$plot_slug = $wp_query->query_vars['fig_plot'];	// Newest x-theme 4.2.x clears post object, need to reset
			$post = FigPlot::GetPlotPost($plot_slug);			
            // Custom Files
            wp_enqueue_style( 'fig-files', plugin_dir_url( __FILE__ ) . 'includes/css/front-end.css' );
            wp_enqueue_script( 'fig-js', plugin_dir_url( __FILE__ ) . 'includes/js/front-end.js', array(), '3.1.0', true );
            // MathJS Library
			wp_enqueue_script( 'math-js', plugin_dir_url( __FILE__ ) . 'includes/js/math.min.js', array(), '2.5.0', true );
            // Responsive Table Lirbrary
            wp_enqueue_style( 'rtables-css', plugin_dir_url( __FILE__ ) . 'includes/js/responsive-tables.css' );
            wp_enqueue_script( 'rtables-js', plugin_dir_url( __FILE__ ) . 'includes/js/responsive-tables.js', array(), '1.0.0', true );
		}
    }
    
}
add_action( 'wp_head', 'fig_in_wp_head' );
// END - Include JS Library in Template --- --- --- 


// Access Database Utils --- --- --- 
function load_access_db_utils() {
    if(!is_admin())   {
		include_once('includes/class-utilities.php');
		include_once('includes/class-fig-plot.php');
		//include_once('includes/class-cama-town.php');
    }
}
add_action('init', 'load_access_db_utils');
// END - Check for Piklist --- --- ---


// Shortcodes --- --- --- 
function shortcode_allfigdata( $atts ){
	$fig_plot = new FigPlot;
	$all_plots = $fig_plot->GetAllPlots();
	$html = '';

	if ( $all_plots->have_posts() ) {
		$html = '<ul class="all-fig-plots">';
		while ( $all_plots->have_posts() ) {
			$all_plots->the_post();
			$sc_plot_bioregion = get_post_meta($all_plots->post->ID, 'fig_plot_bioregion', true);
			$sc_plot_location = get_post_meta($all_plots->post->ID, 'fig_plot_location', true);		
			$html .= '<li><a href="' . get_post_permalink() . '">' . $all_plots->post->post_title . '</a>' .$sc_plot_location . ', ' . $sc_plot_bioregion . '</li>';
		}
		$html .= '</ul>';
	} else {
		$html = '<p>No Plots Available</p>';
	}
	
	wp_reset_postdata();

	return $html;
}
add_shortcode( 'fig_show_all_plots', 'shortcode_allfigdata' );
// END - Shortcodes --- --- --- 


// Override Breadcrumbs code - just use Yoast's since it works.
if ( ! function_exists( 'x_breadcrumbs' ) ) :
    function x_breadcrumbs() {

        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        }
    }
endif;

function qv_isset($var_name) {
    $array = $GLOBALS['wp_query']->query_vars;
    return array_key_exists($var_name, $array);
}

